import math
import numpy
n=3;
Pk=numpy.zeros((n,3));
a=numpy.zeros((n,2));
R=numpy.zeros((n,3));
K=numpy.zeros((n,3));
Pl=numpy.zeros((n,2));
P=numpy.zeros((n,2));
V=numpy.zeros((n,3));
Pi=numpy.zeros((n,2));
A=numpy.zeros((2*n,3));
B=numpy.zeros((1,2*n));

Pk[0,:]=numpy.matrix([0,-500,300]);
a[0,:]=numpy.matrix([5/9*math.pi,0.7293876]);
R[0,:]=numpy.matrix([640,480,1]);
K[0,:]=numpy.matrix([0,1,-1]);
Pl[0,:]=numpy.matrix([320,240]);

Pk[1,:]=numpy.matrix([-500,0,300]);
a[1,:]=numpy.matrix([5/9*math.pi,0.7293876]);
R[1,:]=numpy.matrix([640,480,1]);
K[1,:]=numpy.matrix([1,0,-1]);
Pl[1,:]=numpy.matrix([320,240]);

Pk[2,:]=numpy.matrix([-500,-500,300]);
a[2,:]=numpy.matrix([5/9*math.pi,0.7293876]);
R[2,:]=numpy.matrix([640,480,1]);
K[2,:]=numpy.matrix([1,1,-1]);
Pl[2,:]=numpy.matrix([320,240]);

for i in range(0,n-1):
    R[i,2]=R[i,0]/(2*(math.tan(a[i,1]/2)));
    P[i,0]=math.tan(math.atan2((-Pl[i,0])+R[i,0]/2,R[i,0])+math.atan2(K[i,0],K[i,1]))*R[i,2];
    if (math.atan2((-Pl[i,0])+R[i,0]/2,R[i,2])+math.atan2(K[i,0],K[i,1])==math.pi/2):
        P[i,0]=0; 
        P[i,1]=math.tan(math.atan2((-Pl[i,1])+R[i,1]/2,R[i,1])+math.atan2(K[i,2],K[i,1]))*R[i,2];
    if (math.atan2((-Pl[i,1])+R[i,1]/2,R[i,2])+math.atan2(K[i,2],K[i,1])==math.pi/2):
        P[i,1]=0;
    if ((Pk[i,1]-Pk[i,0])<0) and not((Pk[i,1]-Pk[i,2])==0):
        V[i,:]=numpy.matrix([P[i,0],R[i,2],P[i,1]]); 
    else:
        V[i,:]=numpy.matrix([R[i,2],P[i,0],P[i,1]]);
        
        
for i in range(0,n-1):
    A[i,:]=numpy.matrix([V[i,1],-V[i,0],0]);
    B[0,i]=numpy.matrix(V[i,1]*Pk[i,0]-V[i,0]*Pk[i,1]);
for i in range(0,n-1):

    A[i+n,:]=numpy.matrix([0,V[i,2],-V[i,1]]);
    B[0,i+n]=numpy.matrix(V[i,2]*Pk[i,1]-V[i,1]*Pk[i,2]);

Pi=numpy.linalg.lstsq(A, B.T);
print(Pi);