#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('mvm_cam')
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from gazebo_msgs.msg import LinkStates
from cv_bridge import CvBridge, CvBridgeError

import numpy as np
import math

inp_image = None
outp_image = None

def circles(image, useTrackbars):
    gray = image#cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    param1 = 200
    param2 = 40
    minRadius = 50
    maxRadius = 180
    if useTrackbars:
        param1 = cv2.getTrackbarPos('param1', 'settings')
        param2 = cv2.getTrackbarPos('param2', 'settings')
        minRadius = cv2.getTrackbarPos('minRadius', 'settings')
        maxRadius = cv2.getTrackbarPos('maxRasius', 'settings')
    return cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 20, param1 = param1, param2 = param2, minRadius = minRadius, maxRadius = maxRadius)

def imageWithCircles(image):
    global inp_image
    global outp_image
    detectedCircles = circles(image, False)
    #print(detectedCircles)
    if detectedCircles == None:
        return image
    print(length(detectedCircles))
    for circle in detectedCircles[0, :]:
        a, b, r = circle[0], circle[1], circle[2]
        cv2.circle(image, (a, b), r, (255, 255, 255), 2)
        cv2.circle(image, (a, b), 1, (255, 255, 255), 3) #center
    return image

def circles_tracking(opencv_image):
    global inp_image
    global outp_image
    inp_image = opencv_image
    outp_image = imageWithCircles(opencv_image)

    return outp_image

def findWhiteCircleRadius(image):
    y = 0
    upperDotY = 0
    bottomDotY = 0
    foundUpper = False
    for row in image:
        y += 1
        if 255 in row:
            if not foundUpper:
                upperDotY = y   
                foundUpper = True
            else:
                bottomDotY = y

    return (bottomDotY - upperDotY) / 2          

horizontalFieldOfView = 1.3962634
verticalFieldOfView = horizontalFieldOfView
imageHeightInPixels = 800
focusDistance = imageHeightInPixels / (2 * math.tan(verticalFieldOfView / 2))
sphereHeightInMetres = 0.050 * 2

distanceToSphere = 0
def updateDistanceToSphere(sphereHeightInPixels):
    global distanceToSphere
    distanceToSphere = focusDistance * sphereHeightInMetres / sphereHeightInPixels

sphereRealCoordinates = [0, 0, 0]
def updateSphereRealCoordinates(data):
    global sphereRealCoordinates
    sphereIndex = data.name.index('sphere::sphere_link')
    sphereRealCoordinates = [data.pose[sphereIndex].position.x, data.pose[sphereIndex].position.y, data.pose[sphereIndex].position.z]

cameraRealCoordinates = [0, 0, 0]
def updateCameraRealCoordinates(data):
    global cameraRealCoordinates
    cameraIndex = data.name.index('camera::camera_link')
    cameraRealCoordinates = [data.pose[cameraIndex].position.x, data.pose[cameraIndex].position.y, data.pose[cameraIndex].position.z]

def distance(a, b):
    dx = a[0] - b[0]
    dy = a[1] - b[1]
    dz = a[2] - b[2]
    return math.sqrt(dx * dx + dy * dy + dz * dz)

cameraToSphereRealDistance = 0
def updateCameraToSphereRealDistance(data):
    global cameraToSphereRealDistance
    cameraToSphereRealDistance = distance(sphereRealCoordinates, cameraRealCoordinates)

def updateLinksRealCoordinates(data):
    updateSphereRealCoordinates(data)
    updateCameraRealCoordinates(data)
    updateCameraToSphereRealDistance(data)

links_sub = rospy.Subscriber("/gazebo/link_states", LinkStates, updateLinksRealCoordinates)

def another_color_tracking(opencv_image):
    global inp_image
    global outp_image
    inp_image = opencv_image
    hsv = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2HSV )

    h1 = cv2.getTrackbarPos('h1', 'settings')
    s1 = cv2.getTrackbarPos('s1', 'settings')
    v1 = cv2.getTrackbarPos('v1', 'settings')
    h2 = cv2.getTrackbarPos('h2', 'settings')
    s2 = cv2.getTrackbarPos('s2', 'settings')
    v2 = cv2.getTrackbarPos('v2', 'settings')

    h_min = np.array((h1, s1, v1), np.uint8)
    h_max = np.array((h2, s2, v2), np.uint8)

    thresh = cv2.inRange(hsv, h_min, h_max)
    outp_image = thresh
    return thresh

accept_callback = True

class image_converter:

    def __init__(self):
        #self.object_image = cv2.imread('/home/stepan/red.png')
        self.image_pub = rospy.Publisher("image_topic_2", Image, queue_size=1)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("camera_on_stand/image_raw", Image, self.callback)

    def callback(self,data):
        global accept_callback
        if not accept_callback:
            accept_callback = True
        #    return
        accept_callback = False
        processed_opencv_image = False
        try:
            opencv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            processed_opencv_image = another_color_tracking(opencv_image)

            sphereHeightInPixels = 2 * findWhiteCircleRadius(processed_opencv_image)
            updateDistanceToSphere(sphereHeightInPixels)
            print(focusDistance, sphereHeightInPixels, cameraToSphereRealDistance, distanceToSphere)
            
            out_msg = self.bridge.cv2_to_imgmsg(processed_opencv_image, "8UC1")
            self.image_pub.publish(out_msg)
        except CvBridgeError as e:
            print(e)

def createColorTrackbars():
    cv2.createTrackbar('h1', 'settings', 0, 255, nothing)
    cv2.createTrackbar('s1', 'settings', 0, 255, nothing)
    cv2.createTrackbar('v1', 'settings', 0, 255, nothing)
    cv2.createTrackbar('h2', 'settings', 255, 255, nothing)
    cv2.createTrackbar('s2', 'settings', 255, 255, nothing)
    cv2.createTrackbar('v2', 'settings', 255, 255, nothing)

def createCirclesDetectionParametersrackbars():
    cv2.createTrackbar('param1', 'settings', 1, 200, nothing)
    cv2.createTrackbar('param2', 'settings', 1, 200, nothing)
    cv2.createTrackbar('minRadius', 'settings', 1, 100, nothing)
    cv2.createTrackbar('maxRadius', 'settings', 1, 100, nothing)

def main(args):
    global inp_image
    global outp_image
    cv2.namedWindow( "result" )
    cv2.namedWindow( "settings" )

    createColorTrackbars()
    #createCirclesDetectionParametersrackbars()
    crange = [0,0,0, 0,0,0]
    rospy.init_node('image_converter', anonymous=True)
    ic = image_converter()
    try:
        while not rospy.is_shutdown():
            if inp_image is not None:
                cv2.imshow("settings", inp_image)
            if outp_image is not None:
                cv2.imshow("result", outp_image)
            cv2.waitKey(1)
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    def nothing(*arg):
        pass
    main(sys.argv)
