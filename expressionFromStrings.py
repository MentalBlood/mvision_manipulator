def prefixTree(strings):
    output = {}
    for string in strings:
        string += '0'
        currentNode = output
        for char in string:
            if not (char in currentNode):
                currentNode[char] = {}
            currentNode = currentNode[char]
    return output

def isDict(something):
    return type(something) == type({})

def printPrefixTree(tree, depth = 0):
    if isDict(tree):
        for key in tree:
            print('\t' * depth + key)
            printPrefixTree(tree[key], depth + 1)

strings = ['a', 'ab', 'ac', 'acd', 'acb', 'abd', 'bbd', 'cab', 'ccd', 'bab', 'abdd', 'akb']
tree = prefixTree(strings)
print('\nraw tree:')
print(tree)
print('\nbeautiful tree:')
printPrefixTree(tree)

def getChildrenWhoHaveChildren(node):
    output = []
    if isDict(node):
        for child in node:
            if node[child] != {}:
                output.append(child)
    return output

from itertools import combinations
def getChildrenGroups(children):
    output = []
    for i in range(2, len(children)):
        iChildrenCombinationsList = list(combinations(children, i))
        output += iChildrenCombinationsList
    return output

def listsIntersection(listsList):
    output = None
    for i in range(len(listsList) - 1):
        output = list(set(listsList[i]) & set(listsList[i + 1]))
    return output

def log(string, loggingEnabled):
    if loggingEnabled:
        print(string)

def expressionFromTree(tree, logging = False):
    expression = ''
    for firstChar in tree:
        log('get first char ' + firstChar, logging)
        firstCharNode = tree[firstChar]
        childrenWhoHaveChildren = getChildrenWhoHaveChildren(firstCharNode)
        log('it\'s children who have children: ' + str(childrenWhoHaveChildren), logging)
        if len(childrenWhoHaveChildren) > 1:
            log('their amount is more than 1, so continuing', logging)
            childrenGroups = getChildrenGroups(childrenWhoHaveChildren)
            log('children groups are ' + str(childrenGroups), logging)
            for childrenGroup in childrenGroups:
                log('processing children group ' + str(childrenGroup), logging)
                childrenChildren = []
                for child in childrenGroup:
                    childrenChildren.append(list(firstCharNode[child].keys()))
                log('children children are ' + str(childrenChildren), logging)
                jointChildren = listsIntersection(childrenChildren)
                log('joint children are ' + str(jointChildren), logging)
                if not jointChildren:
                    log('no joint children, so ending processing this group', logging)
                    continue
                expression += firstChar + '(' + '|'.join(childrenGroup) + ')'
                if len(jointChildren) > 1:
                    expression += '(' + '|'.join(jointChildren) + ')'
                else:
                    expression += '|'.join(jointChildren)
                expression += '|'
                log('now expression is ' + expression[:-1], logging)
    return expression[:-1]
                

print()
expression = expressionFromTree(tree, True)
print('\nexpression:')
print(expression)